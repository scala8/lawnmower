package action

import exceptions.DonneesIncorrectesException

sealed trait Action extends Product with Serializable

object Action {
  @SuppressWarnings(Array("org.wartremover.warts.Throw"))
  def apply(letter: Char): Action = letter match {
    case 'A' => Avancer
    case 'G' => Gauche
    case 'D' => Droite
    case other =>
      throw DonneesIncorrectesException(
        s"La lettre '${other.toString}' n'est pas reconnue comme une action valide"
      )
  }
}

case object Avancer extends Action
case object Gauche extends Action
case object Droite extends Action
