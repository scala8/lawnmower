package exceptions

case class DonneesIncorrectesException(
    private val message: String
) extends Exception(message)
