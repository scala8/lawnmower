package exceptions

case class ArgumentManquantException(
    private val message: String
) extends Exception(message)
