package exceptions

case class OperationInconnueException(
    private val message: String
) extends Exception(message)
