package jsonSerializer

import action.{Action, Avancer, Droite, Gauche}
import directions.{Direction, East, North, South, West}
import lawnmower.{Grille, Point, PointOrientation, Tondeuse}

trait JsonSerializer[A] {
  def toJson(x: A): String
}

object JsonSerializer {
  implicit val toJsonInt: JsonSerializer[Int] = new JsonSerializer[Int] {
    override def toJson(x: Int): String = s"${x.toString}"
  }

  implicit val toJsonString: JsonSerializer[String] =
    new JsonSerializer[String] {
      override def toJson(x: String): String = "\"" + x + "\""
    }

  implicit val toJsonChar: JsonSerializer[Char] = new JsonSerializer[Char] {
    override def toJson(x: Char): String = "\"" + x.toString + "\""
  }

  implicit val toJsonListChar: JsonSerializer[List[Char]] =
    new JsonSerializer[List[Char]] {
      override def toJson(x: List[Char]): String =
        "[" + x.map(c => toJsonChar.toJson(c)).mkString(", ") + "]"
    }

  implicit val toJsonListDirection: JsonSerializer[List[Direction]] =
    new JsonSerializer[List[Direction]] {
      override def toJson(x: List[Direction]): String =
        "[" + x.map(d => toJsonDirection.toJson(d)).mkString(", ") + "]"
    }

  implicit val toJsonListAction: JsonSerializer[List[Action]] =
    new JsonSerializer[List[Action]] {
      override def toJson(x: List[Action]): String =
        "[" + x.map(a => toJsonAction.toJson(a)).mkString(", ") + "]"
    }

  implicit val toJsonPoint: JsonSerializer[Point] = new JsonSerializer[Point] {
    override def toJson(x: Point): String =
      s"""{
         |\t"x": ${toJsonInt.toJson(x.x)},
         |\t"y": ${toJsonInt.toJson(x.y)}
         |}""".stripMargin
  }

  implicit val toJsonDirection: JsonSerializer[Direction] =
    new JsonSerializer[Direction] {
      override def toJson(x: Direction): String = x match {
        case North => toJsonChar.toJson('N')
        case South => toJsonChar.toJson('S')
        case East  => toJsonChar.toJson('E')
        case West  => toJsonChar.toJson('W')
      }
    }

  implicit val toJsonListTondeuse: JsonSerializer[List[Tondeuse]] =
    new JsonSerializer[List[Tondeuse]] {
      override def toJson(x: List[Tondeuse]): String =
        s"""[
         |${x.map(t => toJsonTondeuse.toJson(t)).mkString(",\n")}
         |]""".stripMargin
    }

  implicit val toJsonAction: JsonSerializer[Action] =
    new JsonSerializer[Action] {
      override def toJson(x: Action): String = x match {
        case Avancer => toJsonChar.toJson('A')
        case Gauche  => toJsonChar.toJson('G')
        case Droite  => toJsonChar.toJson('D')
      }
    }

  implicit val toJsonPointOrientation: JsonSerializer[PointOrientation] =
    new JsonSerializer[PointOrientation] {
      override def toJson(x: PointOrientation): String =
        s"""{
         |\t"point": ${toJsonPoint.toJson(x.point)},
         |\t"direction": ${toJsonDirection.toJson(x.orientation)}
         |}""".stripMargin
    }

  implicit val toJsonTondeuse: JsonSerializer[Tondeuse] =
    new JsonSerializer[Tondeuse] {
      override def toJson(x: Tondeuse): String =
        s"""{
        |\t"debut": ${toJsonPointOrientation.toJson(x.positionInitiale)},
        |\t"instructions": ${toJsonListAction.toJson(x.instructionsInitials)},
        |\t"fin": ${toJsonPointOrientation.toJson(x.position)}
        |}""".stripMargin
    }

  implicit val toJsonGrille: JsonSerializer[Grille] =
    new JsonSerializer[Grille] {
      override def toJson(x: Grille): String =
        s"""{
         |\t"limite": ${toJsonPoint.toJson(x.limite)},
         |\t"tondeuses": ${toJsonListTondeuse.toJson(x.finishedTondeuses)}
         |}""".stripMargin
    }

  def toJson[A](x: A)(implicit jsonA: JsonSerializer[A]): String =
    jsonA.toJson(x)
}
