package parserUtils

import action.Action
import directions.Direction
import exceptions.DonneesIncorrectesException
import lawnmower.{Point, PointOrientation, Tondeuse, TondeuseInput}

import scala.util.matching.Regex

class TextParser(private val lines: List[String]) {
  private val limite: Regex = """^(\d+)\s(\d+)$""".r
  private val positionInitial: Regex = """^(\d+) (\d+) ([NEWS])$""".r
  private val listeInstructions: Regex = """^([DGA]+)$""".r

  @SuppressWarnings(Array("org.wartremover.warts.Throw"))
  val getPointLimite: Point = lines.headOption.getOrElse("") match {
    case this.limite(x, y) => Point(x.toInt, y.toInt)
    case _ =>
      throw DonneesIncorrectesException(
        "La première ligne ne correspond pas à une taille de Grille valide."
      )
  }

  val getTondeuses: List[Tondeuse] =
    generateTondeusesInput(lines.drop(1)).map(handleTondeuseInput)

  @SuppressWarnings(Array("org.wartremover.warts.Throw"))
  private def handleTondeuseInput(tondeuseInput: TondeuseInput): Tondeuse = {
    val p: PointOrientation = tondeuseInput.position match {
      case this.positionInitial(x, y, o) =>
        PointOrientation(
          Point(x.toInt, y.toInt),
          Direction(o.charAt(0))
        )
      case _ =>
        throw DonneesIncorrectesException(
          s"""Erreur à l'input : ${tondeuseInput.toString} La position initiale ne correspond pas au format attendu"""
        )
    }
    val instructions: List[Action] = tondeuseInput.instructions match {
      case this.listeInstructions(instructions) =>
        instructions.toList.map(c => Action(c))
      case _ =>
        throw DonneesIncorrectesException(
          s"""Erreur à l'input : ${tondeuseInput.toString} La liste d'instructions ne correspond pas au format attendu"""
        )
    }
    Tondeuse(p, p, instructions, instructions)
  }

  @SuppressWarnings(Array("org.wartremover.warts.Throw"))
  private def generateTondeusesInput(lines: List[String]): List[TondeuseInput] =
    lines match {
      case Nil => Nil
      case position :: instructions :: tail =>
        TondeuseInput(position, instructions) :: generateTondeusesInput(tail)
      case _ =>
        throw DonneesIncorrectesException(
          """Les données d'entrées pour les tondeuses contiennent un nombre
        | incohérent de ligne. Vérifiez que vous disposez bien de 2 lignes par tondeuse""".stripMargin
        )
    }

}
