package lawnmower

import action.{Avancer, Droite, Gauche}
import directions.Direction
import exceptions.OperationInconnueException

import scala.annotation.tailrec

class Grille(val limite: Point, private val tondeuses: List[Tondeuse]) {
  @tailrec
  @SuppressWarnings(Array("org.wartremover.warts.Throw"))
  private def handleTondeuse(tondeuse: Tondeuse): Tondeuse =
    tondeuse.instructions match {
      case Nil => tondeuse
      case Avancer :: tail =>
        handleTondeuse(
          move(tondeuse, tondeuse.position.orientation).withInstructions(tail)
        )
      case Gauche :: tail =>
        handleTondeuse(
          tondeuse
            .withInstructions(tail)
            .withPosition(
              tondeuse.position
                .copy(orientation = tondeuse.position.orientation.gauche)
            )
        )
      case Droite :: tail =>
        handleTondeuse(
          tondeuse
            .withInstructions(tail)
            .withPosition(
              tondeuse.position
                .copy(orientation = tondeuse.position.orientation.droite)
            )
        )
      case other =>
        throw OperationInconnueException(
          s"L'opération ${other.toString} n'est pas reconnue !"
        )
    }

  private def move(tondeuse: Tondeuse, direction: Direction): Tondeuse = {
    val newPoint = tondeuse.position.point.move(direction)
    if (isOutofBounds(newPoint)) {
      tondeuse
    } else {
      tondeuse.withPosition(tondeuse.position.copy(point = newPoint))
    }
  }

  private def isOutofBounds(point: Point): Boolean =
    point.x < 0 || point.y < 0 || point.x > limite.x || point.y > limite.y

  val finishedTondeuses: List[Tondeuse] = tondeuses.map(handleTondeuse)
}
