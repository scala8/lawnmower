package lawnmower

import directions._

case class Point(x: Int, y: Int) {
  def move(direction: Direction): Point = direction match {
    case North => copy(y = y + 1)
    case South => copy(y = y - 1)
    case West  => copy(x = x - 1)
    case East  => copy(x = x + 1)
  }
}
