package lawnmower

import directions.Direction

case class PointOrientation(point: Point, orientation: Direction)
