package lawnmower

import action.Action

case class Tondeuse(
    positionInitiale: PointOrientation,
    position: PointOrientation,
    instructionsInitials: List[Action],
    instructions: List[Action]
) {
  def withPosition(position: PointOrientation): Tondeuse =
    copy(position = position)

  def withInstructions(instructions: List[Action]): Tondeuse =
    copy(instructions = instructions)
}
