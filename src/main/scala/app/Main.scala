package app

import exceptions.ArgumentManquantException
import fileUtils.{FileReader, FileWriter}
import jsonSerializer.JsonSerializer
import lawnmower.Grille
import parserUtils.TextParser

@SuppressWarnings(Array("org.wartremover.warts.Throw"))
object Main extends App {
  if (args.length != 2) {
    throw ArgumentManquantException(
      "Merci de fournir les deux arguments suivants: fichier de configuration, fichier de sortie en json"
    )
  } else {
    val lines = FileReader.readFile(args(0))
    val parser = new TextParser(lines)
    val grille = new Grille(parser.getPointLimite, parser.getTondeuses)
    FileWriter.writeFile(args(1), JsonSerializer.toJson(grille))
  }
}
