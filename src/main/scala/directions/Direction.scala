package directions

import exceptions.DonneesIncorrectesException

sealed trait Direction {
  def gauche: Direction
  def droite: Direction
}

object Direction {
  @SuppressWarnings(Array("org.wartremover.warts.Throw"))
  def apply(letter: Char): Direction = letter match {
    case 'N' => North
    case 'S' => South
    case 'W' => West
    case 'E' => East
    case other =>
      throw DonneesIncorrectesException(
        s"La lettre '${other.toString}' n'est pas reconnue comme une orientation valide"
      )
  }
}

case object North extends Direction {
  override def gauche: Direction = West

  override def droite: Direction = East
}

case object South extends Direction {
  override def gauche: Direction = East

  override def droite: Direction = West
}

case object West extends Direction {
  override def gauche: Direction = South

  override def droite: Direction = North
}

case object East extends Direction {
  override def gauche: Direction = North

  override def droite: Direction = South
}
