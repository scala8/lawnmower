package fileUtils

import scala.io.Source

object FileReader {
  def readFile(filename: String): List[String] = {
    val file = Source.fromFile(filename)
    val lines = file.getLines().toList
    file.close()
    lines
  }
}
