package lawnmower

import action.{Avancer, Gauche}
import directions.North
import org.scalatest.funsuite.AnyFunSuite

class TondeuseSpec extends AnyFunSuite {

  test("should return copy with new position") {
    val tondeuse = Tondeuse(
      positionInitiale = PointOrientation(Point(2, 2), North),
      position = PointOrientation(Point(2, 2), North),
      instructionsInitials = List(Avancer),
      instructions = List(Avancer)
    )
    val tondeusePositionModified = Tondeuse(
      positionInitiale = PointOrientation(Point(2, 2), North),
      position = PointOrientation(Point(2, 3), North),
      instructionsInitials = List(Avancer),
      instructions = List(Avancer)
    )
    assertResult(tondeusePositionModified)(
      tondeuse.withPosition(PointOrientation(Point(2, 3), North))
    )
  }

  test("should return copy with new instructions") {
    val tondeuse = Tondeuse(
      positionInitiale = PointOrientation(Point(2, 2), North),
      position = PointOrientation(Point(2, 2), North),
      instructionsInitials = List(Avancer, Gauche),
      instructions = List(Avancer, Gauche)
    )
    val tondeuseInstructionModified = Tondeuse(
      positionInitiale = PointOrientation(Point(2, 2), North),
      position = PointOrientation(Point(2, 2), North),
      instructionsInitials = List(Avancer, Gauche),
      instructions = List(Gauche)
    )
    assertResult(tondeuseInstructionModified)(
      tondeuse.withInstructions(List(Gauche))
    )
  }
}
