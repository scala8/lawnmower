package lawnmower

import directions.{East, North, South, West}
import org.scalatest.funsuite.AnyFunSuite

class PointSpec extends AnyFunSuite {

  test("should move point to North when direction North given") {
    assertResult(Point(2, 3))(Point(2, 2).move(North))
  }

  test("should move point to South when direction South given") {
    assertResult(Point(2, 1))(Point(2, 2).move(South))
  }

  test("should move point to West when direction West given") {
    assertResult(Point(1, 2))(Point(2, 2).move(West))
  }

  test("should move point to East when direction East given") {
    assertResult(Point(3, 2))(Point(2, 2).move(East))
  }
}
