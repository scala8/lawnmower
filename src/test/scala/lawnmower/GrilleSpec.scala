package lawnmower

import action.{Avancer, Droite, Gauche}
import directions.{East, North, West}
import org.scalatest.funsuite.AnyFunSuite

class GrilleSpec extends AnyFunSuite {

  test("should execute action of the lawnmower") {
    val tondeuse = Tondeuse(
      positionInitiale = PointOrientation(Point(2, 2), North),
      position = PointOrientation(Point(2, 2), North),
      instructionsInitials = List(Avancer, Gauche),
      instructions = List(Avancer, Gauche)
    )

    val grille = new Grille(Point(5, 5), List(tondeuse))

    val tondeuseFinal = Tondeuse(
      positionInitiale = PointOrientation(Point(2, 2), North),
      position = PointOrientation(Point(2, 3), West),
      instructionsInitials = List(Avancer, Gauche),
      instructions = List()
    )

    assertResult(List(tondeuseFinal))(grille.finishedTondeuses)
  }

  test("should execute action for two lawnmowers") {
    val tondeuseOne = Tondeuse(
      positionInitiale = PointOrientation(Point(2, 2), North),
      position = PointOrientation(Point(2, 2), North),
      instructionsInitials = List(Avancer, Gauche),
      instructions = List(Avancer, Gauche)
    )

    val tondeuseTwo = Tondeuse(
      positionInitiale = PointOrientation(Point(3, 2), North),
      position = PointOrientation(Point(2, 2), North),
      instructionsInitials = List(Avancer, Droite, Avancer),
      instructions = List(Avancer, Droite, Avancer)
    )

    val grille = new Grille(Point(5, 5), List(tondeuseOne, tondeuseTwo))

    val tondeuseOneFinal = Tondeuse(
      positionInitiale = PointOrientation(Point(2, 2), North),
      position = PointOrientation(Point(2, 3), West),
      instructionsInitials = List(Avancer, Gauche),
      instructions = List()
    )

    val tondeuseTwoFinal = Tondeuse(
      positionInitiale = PointOrientation(Point(3, 2), North),
      position = PointOrientation(Point(3, 3), East),
      instructionsInitials = List(Avancer, Droite, Avancer),
      instructions = List()
    )

    assertResult(List(tondeuseOneFinal, tondeuseTwoFinal))(
      grille.finishedTondeuses
    )
  }

}
