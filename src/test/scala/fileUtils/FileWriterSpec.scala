package fileUtils

import org.scalatest.funsuite.AnyFunSuite

import java.io.File
import scala.io.Source

class FileWriterSpec extends AnyFunSuite {

  test("writing to the temp file") {
    FileWriter.writeFile("output.json", """{"test": "test1"}""")

    val bufferedSource = Source.fromFile("output.json")
    val lines = bufferedSource.getLines().toList
    bufferedSource.close()

    val file = new File("output.json")

    assert(file.delete())

    assertResult(List("""{"test": "test1"}"""))(
      lines
    )
  }

}
