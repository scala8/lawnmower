package fileUtils

import org.scalatest.funsuite.AnyFunSuite

import java.io.{BufferedWriter, File, FileWriter}

class FileReaderSpec extends AnyFunSuite {

  test("reading from the temp file") {
    val file = new File("test.txt")
    val bw = new BufferedWriter(new FileWriter(file))
    bw.write("5 5\n2 2 N\nAG\n3 2 N\nADA")
    bw.close()

    val result = FileReader.readFile("test.txt")

    assert(file.delete())

    assertResult(List("5 5", "2 2 N", "AG", "3 2 N", "ADA"))(
      result
    )
  }

}
