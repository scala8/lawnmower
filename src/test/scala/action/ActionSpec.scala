package action

import org.scalatest.funsuite.AnyFunSuite

class ActionSpec extends AnyFunSuite {

  test("should return Avancer when A given") {
    assertResult(Avancer)(Action('A'))
  }

  test("should return Gauche when G given") {
    assertResult(Gauche)(Action('G'))
  }

  test("should return Droite when D given") {
    assertResult(Droite)(Action('D'))
  }

  test(
    """should throw a "exceptions.DonneesIncorrectesException" when not A, G or D"""
  ) {
    assertThrows[exceptions.DonneesIncorrectesException](Action('F'))
  }

}
