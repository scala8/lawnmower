package parserUtils

import action.{Avancer, Droite, Gauche}
import directions.North
import lawnmower.{Point, PointOrientation, Tondeuse}
import org.scalatest.funsuite.AnyFunSuite

class TextParserSpec extends AnyFunSuite {

  test("should return list of lawnmower") {
    val lines = List(
      "5 5",
      "0 0 N",
      "AAADA",
      "2 0 N",
      "AAGAD"
    )

    val textParser = new TextParser(lines)

    val tondeuseOne = Tondeuse(
      positionInitiale = PointOrientation(Point(0, 0), North),
      position = PointOrientation(Point(0, 0), North),
      instructionsInitials = List(Avancer, Avancer, Avancer, Droite, Avancer),
      instructions = List(Avancer, Avancer, Avancer, Droite, Avancer)
    )
    val tondeuseTwo = Tondeuse(
      positionInitiale = PointOrientation(Point(2, 0), North),
      position = PointOrientation(Point(2, 0), North),
      instructionsInitials = List(Avancer, Avancer, Gauche, Avancer, Droite),
      instructions = List(Avancer, Avancer, Gauche, Avancer, Droite)
    )

    assertResult(List(tondeuseOne, tondeuseTwo))(textParser.getTondeuses)
  }

  test("should return point limit") {
    val lines = List(
      "5 5",
      "0 0 N",
      "AAADA",
      "2 0 N",
      "AAGAD"
    )

    val textParser = new TextParser(lines)

    assertResult(Point(5, 5))(textParser.getPointLimite)
  }

  test(
    """should throw a "exceptions.DonneesIncorrectesException" when not good first line"""
  ) {
    val lines = List(
      "5 ",
      "0 0 N",
      "AAADA",
      "2 0 N",
      "AAGAD"
    )

    assertThrows[exceptions.DonneesIncorrectesException](new TextParser(lines))
  }

  test(
    """should throw a "exceptions.DonneesIncorrectesException" when not good position line"""
  ) {
    val lines = List(
      "5 5",
      "0 0",
      "AAADA",
      "2 0 N",
      "AAGAD"
    )

    assertThrows[exceptions.DonneesIncorrectesException](new TextParser(lines))
  }

  test(
    """should throw a "exceptions.DonneesIncorrectesException" when not good instructions line"""
  ) {
    val lines = List(
      "5 5",
      "0 0 N",
      "AAADA 55",
      "2 0 N",
      "AAGAD"
    )

    assertThrows[exceptions.DonneesIncorrectesException](new TextParser(lines))
  }

  test(
    """should throw a "exceptions.DonneesIncorrectesException" when not 2 lines per lawnmower"""
  ) {
    val lines = List(
      "5 5",
      "0 0 N",
      "AAADA 55",
      "2 0 N"
    )

    assertThrows[exceptions.DonneesIncorrectesException](new TextParser(lines))
  }
}
