package directions

import org.scalatest.funsuite.AnyFunSuite

class DirectionSpec extends AnyFunSuite {

  test("should return North when N given") {
    assertResult(North)(Direction('N'))
  }

  test("should return South when S given") {
    assertResult(South)(Direction('S'))
  }

  test("should return West when W given") {
    assertResult(West)(Direction('W'))
  }

  test("should return East when E given") {
    assertResult(East)(Direction('E'))
  }

  test(
    """should throw a "exceptions.DonneesIncorrectesException" when not A, G or D"""
  ) {
    assertThrows[exceptions.DonneesIncorrectesException](Direction('F'))
  }

}
