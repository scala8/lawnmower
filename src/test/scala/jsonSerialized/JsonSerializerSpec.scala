package jsonSerialized

import action.{Avancer, Droite, Gauche}
import directions.North
import jsonSerializer.JsonSerializer
import lawnmower.{Grille, Point, PointOrientation, Tondeuse}
import org.scalatest.funsuite.AnyFunSuite

class JsonSerializerSpec extends AnyFunSuite {

  test("should format to json") {
    val tondeuseOne = Tondeuse(
      positionInitiale = PointOrientation(Point(2, 2), North),
      position = PointOrientation(Point(2, 2), North),
      instructionsInitials = List(Avancer, Gauche),
      instructions = List(Avancer, Gauche)
    )

    val tondeuseTwo = Tondeuse(
      positionInitiale = PointOrientation(Point(3, 2), North),
      position = PointOrientation(Point(3, 2), North),
      instructionsInitials = List(Avancer, Droite, Avancer),
      instructions = List(Avancer, Droite, Avancer)
    )

    val grille = new Grille(Point(5, 5), List(tondeuseOne, tondeuseTwo))

    assertResult(
      "{\n\t\"limite\": {\n\t\"x\": 5,\n\t\"y\": 5\n},\n\t\"tondeuses\": [\n{\n\t\"debut\": {\n\t\"point\": {\n\t\"x\": 2,\n\t\"y\": 2\n},\n\t\"direction\": \"N\"\n},\n\t\"instructions\": [\"A\", \"G\"],\n\t\"fin\": {\n\t\"point\": {\n\t\"x\": 2,\n\t\"y\": 3\n},\n\t\"direction\": \"W\"\n}\n},\n{\n\t\"debut\": {\n\t\"point\": {\n\t\"x\": 3,\n\t\"y\": 2\n},\n\t\"direction\": \"N\"\n},\n\t\"instructions\": [\"A\", \"D\", \"A\"],\n\t\"fin\": {\n\t\"point\": {\n\t\"x\": 4,\n\t\"y\": 3\n},\n\t\"direction\": \"E\"\n}\n}\n]\n}"
    )(JsonSerializer.toJson(grille))
  }
}
